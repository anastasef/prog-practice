# Programming Practice #

Practical excercises from [500 Data Structures and Algorithms practice problems and their solutions](https://medium.com/@kingrayhan/500-data-structures-and-algorithms-practice-problems-and-their-solutions-b45a83d803f0) and other sources.

**Themes to be covered:**

* Data structures
* Algorithms
* Puzzles

## How to check puzzles? ##

Each puzzle has a JUnit4 test class under `src/test/java/nast`. You will only need to launch it.

## On tests

Under test directory you will find a `test` package. It is thematically separate 
from the other tests and it's purpose was to try TDD. Maybe I will move it to another repo in the future.

### 1. Calculator

[Video tutorial: "How to write TDD Unit Tests in Java - Arrange, Act, Assert"](https://www.youtube.com/watch?v=d1EAyR_NCOA)

Path: `src/test/java/nast/CalculatorTest.java`

What the `Calculator` class does:

- adds 2 numbers;
- pulls these numbers from source;
- dependency injection is used on that source.

### 2. TemplateTest

Is not that interesting; I used it to get my feet wet with the TDD approach.
