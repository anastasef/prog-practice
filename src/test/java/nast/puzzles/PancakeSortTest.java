package nast.puzzles;

import static org.junit.Assert.*;

import org.junit.Test;

public class PancakeSortTest {
	
	PancakeSort pan = new PancakeSort();
	
	// Flip test ---------------------------------
	
	@Test
	public void testFlip_EvenArray_fullFlip() {
		
		int[] arrEven = { 0, 1, 2, 3, 4, 5 };
		
		assertArrayEquals(new int[] { 5, 4, 3, 2, 1, 0 }, pan.flip(arrEven, arrEven.length));
	}
	
	@Test
	public void testFlip_EvenArray_partialFlip() {
		
		int[] arrEven = { 0, 1, 2, 3, 4, 5 };
		
		assertArrayEquals(new int[] { 3, 2, 1, 0, 4, 5 }, pan.flip(arrEven, 3));
	}
	
	@Test
	public void testFlip_EvenArray_fullFlip_kIsBiggerThanLength() {
		
		int[] arrEven = { 0, 1, 2, 3, 4, 5 };
		
		assertArrayEquals(new int[] { 5, 4, 3, 2, 1, 0 }, pan.flip(arrEven, 20));
	}
	
	@Test
	public void testFlip_OddArray_fullFlip() {
		
		int[] arrOdd = { 0, 1, 2, 3, 4 };
		
		assertArrayEquals(new int[] { 4, 3, 2, 1, 0 }, pan.flip(arrOdd, arrOdd.length));
	}
	
	@Test
	public void testFlip_PairArray_fullFlip() {
		
		int[] arrPair = { 0, 1 };
		
		assertArrayEquals(new int[] { 1, 0 }, pan.flip(arrPair, arrPair.length));
	}
	
	@Test
	public void testFlip_EmptyArray() {
		
		int[] arrEmpty = {};
		
		assertArrayEquals(new int[] {}, pan.flip(arrEmpty, arrEmpty.length));
	}
	
	// Sort test ---------------------------------

	@Test
	public void testSort() {
		
		int[] arr = { 65, 1, 26, 5, 34, 0, 25 };
		
		assertArrayEquals(new int[] { 0, 1, 5, 25, 26, 34, 65 }, pan.pancakeSort(arr));
	}
	
	@Test
	public void testSort_pair() {
		
		int[] arr = { 65, 1 };
		
		assertArrayEquals(new int[] { 1, 65 }, pan.pancakeSort(arr));
	}
	
}
