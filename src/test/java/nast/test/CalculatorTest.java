package nast.test;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class CalculatorTest {

	private static final long SECOND_NUMBER = 12345L;
	private static final long FIRST_NUMBER = 4567L;
	
	@Mock
	private NumberSource source;
	
	private AutoCloseable closeable;

    @Before 
    public void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        
        Mockito.when(source.fetchNextNumber()).thenReturn(FIRST_NUMBER, SECOND_NUMBER);
    }

    @After 
    public void releaseMocks() throws Exception {
        closeable.close();
    }

	@Test
	public void addTwoNumbers() {
		
		Calculator calculator = new Calculator(source);
		
		long result = calculator.addTwoNumbers();
		
		assertEquals(result, (FIRST_NUMBER + SECOND_NUMBER));
	}
}
