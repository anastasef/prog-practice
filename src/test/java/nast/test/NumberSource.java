package nast.test;

public interface NumberSource {

	long fetchNextNumber();

}
