package nast.test;

public class Calculator {

	private final NumberSource numberSource;
	
	public Calculator(final NumberSource numberSource) {
		this.numberSource = numberSource;
	}

	public long addTwoNumbers() {
		return numberSource.fetchNextNumber() + numberSource.fetchNextNumber();		
	}

}
