package nast.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TemplateTest {
	
	private String thePattern;

	@Before
	public void setUp() throws Exception {
		thePattern = "Your test score was: %d";
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("tearing down");
	}

	@Test
	public void canInstantiateATemplate() {
		Template template = new Template("f");
		assertNotNull(template);
	}
	
	@Test
	public void renderMethodReturnsAString() {
		Template template = new Template("Your test score was: ");
		assertNotNull(template.renderTemplate(1));
	}
	
	@Test
	public void renderMethodReturnsProperMessageGivenScore() {
		Template template = new Template(thePattern);
		int score = 1;
		String expected = "Your test score was: " + score;
		
		assertEquals(expected, template.renderTemplate(score));
	}
	
}
