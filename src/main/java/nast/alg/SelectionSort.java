package nast.alg;

public class SelectionSort {
	
	public static void main(String[] args) {
        
        int[] numbers = { 3, 6, 1, 7, 2, 8, 10, 4, 9, 5 };
        int len = numbers.length;
        
        selectionSort(numbers, 0, len-1);
        
        for (int i = 0; i < len; i++) System.out.print(numbers[i] + " ");
        
	}
    
    public static void selectionSort(int[] numbers, int start, int end) {
        for (int i = start; i <= end; i++) {
            swap(numbers, i, getSmallestPos(numbers, i, end));
        }
    }
    
    /**
     * Get position of the smallest value 
     * in numbers[start] to numbers[end]
     */
    public static int getSmallestPos(int[] numbers, int start, int end) {
        int sm = start;
        for (int i = start+1; i <= end; i++) {
            if (numbers[i] < numbers[sm]) sm = i;
        }
        return sm;
    }
    
    public static void swap(int[] numbers, int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }
}
