package nast.alg;

public class InsertionSort {

	public static void main(String[] args) {
		int[] unsorted = { 3, 6, 1, 7, 2, 8, 10, 4, 9, 5 };
		insertionSort(unsorted);
		
		for (int i = 0; i < unsorted.length; i++) System.out.print(unsorted[i] + " ");
	}
	
	public static void insertionSort(int[] arr) {
		
		for (int i = 1; i < arr.length; i++) {
			int current = arr[i];
			int prevIndex = i - 1;
			
			while ((prevIndex > -1) && (arr[prevIndex] > current)) {
				arr[prevIndex+1] = arr[prevIndex];
				prevIndex--;
			}
			
			arr[prevIndex+1] = current;
		}
	}
	
}
