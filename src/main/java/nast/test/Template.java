package nast.test;

public class Template {

	private String templateText;

	public Template(String s) {
		this.templateText = s;
	}
	
	public String renderTemplate(int score) {
		return String.format(this.templateText, score);
	}
	
	
}
