package nast.puzzles;


/**
 * Link to the puzzle:
 * https://leetcode.com/problems/pancake-sorting/
 * 
 * Given an array of integers arr, sort the array by performing a series of pancake flips.
 * In one pancake flip we do the following steps:
 *
 *  1. Choose an integer k where 1 <= k <= arr.length.
 *  2. Reverse the sub-array arr[1...k].
 *
 * For example, if arr = [3,2,1,4] and we performed a pancake flip choosing k = 3, 
 * we reverse the sub-array [3,2,1], so arr = [1,2,3,4] after the pancake flip at k = 3.
 *
 * Return the k-values corresponding to a sequence of pancake flips that sort arr. 
 * Any valid answer that sorts the array within 10 * arr.length flips will be judged as correct.
 *
 */
public class PancakeSort {

	public int[] pancakeSort(int[] arr) {
		
		if (arr[0] > arr[arr.length-1]) {
			flip(arr, arr.length-1);
		}
		
		for (int i = 0; i < arr.length-1; i++) {
			
			if (arr[i] > arr[i+1]) {
				flip(arr, i);
				flip(arr, i+1);
				i = 0;
			}
		}
		
		return arr;
	}
	
	public int[] flip(int[] arr, int k) {

		if (arr.length == 0 || arr.length < 2) return arr;
		if (k >= arr.length) k = arr.length - 1;
		int tmp;
		
		// i moves from left to right
		// k-1 is the last element
		// k/2 is the index of
		//   (1) the central element
		//   (2) the right element of the central pair
		// k-1-i is the index of the element, symmetrical to the i when going from right to left
		
		// 0 1 2 3 4 5 | k=len=6 k/2=3
		// 0 1 2 3 4   | k=len=5 k/2=2
		
		
		for (int i = 0; i < k/2+1; i++) {
			tmp = arr[i];
			arr[i] = arr[k-i];
			arr[k-i] = tmp;
		}
		
		return arr;
	}
}
